<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/5/9
  Time: 下午9:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>用户列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath}/static/plugins/layui/css/layui.css"  media="all">
</head>
<body>
<div id="app" class="layui-form" >
    <div class="layui-form-item">
        <label class="layui-form-label">姓名</label>
        <div class="layui-input-inline">
            <input type="text" name="title" required  lay-verify="required" placeholder="请输入姓名" autocomplete="off" class="layui-input">
        </div>
        <label class="layui-form-label">电话</label>
        <div class="layui-input-inline">
            <input type="text" name="title" required  lay-verify="required" placeholder="请输入电话" autocomplete="off" class="layui-input">
        </div>
        <label class="layui-form-label">邮箱</label>
        <div class="layui-input-inline">
            <input type="text" name="title" required  lay-verify="required" placeholder="请输入电话" autocomplete="off" class="layui-input">
        </div>
        <label class="layui-form-label">生日</label>
        <div class="layui-inline">
            <input class="layui-input" placeholder="自定义日期格式" onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
        </div>
    </div>
    <button class="layui-btn" @click="search_user">查询</button>
    <button class="layui-btn" @click="create_user">添加</button>
    <table class="layui-table">
        <colgroup>
            <col width="50">
            <col width="150">
            <col width="150">
            <col width="200">
            <col width="200">
            <col width="200">
            <col >
        </colgroup>
        <thead>
        <tr>
            <th><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose"></th>
            <th>姓名</th>
            <th>性别</th>
            <th>电话</th>
            <th>生日</th>
            <th>邮箱</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <tr v-for="(user,index) in userlist">
            <td><input type="checkbox" name="" lay-skin="primary"></td>
            <td>{{user.user_name}}</td>
            <td>{{user.user_sex}}</td>
            <td>{{user.user_phone}}</td>
            <td>{{user.user_birthday}}</td>
            <td>{{user.user_email}}</td>
            <td>
                <button class="layui-btn" @click="delete_user(user.user_pk)">
                    <i class="layui-icon">&#xe642;</i>
                </button>
                <button class="layui-btn" @click="delete_user(user.user_pk)">
                    <i class="layui-icon">&#xe640;</i>
                </button>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>

<script src="${basePath}/static/plugins/jquery/jquery-3.2.1.min.js"></script>
<script src="${basePath}/static/plugins/vuejs/vue.js"></script>
<script src="${basePath}/static/plugins/vuejs/vue-resource.min.js"></script>
<script src="${basePath}/static/plugins/layui/layui.js"></script>
<script>
    var vm = new Vue({
        el: '#app',
        data: {
            user: {
                user_pk:'',
                user_name: '',
                user_sex: '',
                user_phone: '',
                user_birthday: '',
                user_email: ''
            },
            userlist:[]
        },
        mounted: function () {
            var url = '${basePath}/user/list';
            this.$http.get(url).then(function(response){
                console.info(response);
                this.userlist = response.body;
            },function(response){
                console.info(response);
            })
        },
        methods:{
            //向列表中添加一条数据
            create_user: function(){
                this.userlist.push(this.user);
                // 添加完newPerson对象后，重置newPerson对象
                this.user = {
                    user_pk: '',
                    user_name: '',
                    user_sex: '',
                    user_phone: '',
                    user_birthday: '',
                    user_email: ''
                }
            },
            //从列表中删除数据
            delete_user: function(user_pk){
                // 删一个数组元素
                <%--console.log(user_pk);--%>
                <%--var url = '${basePath}/user/delete';--%>
                <%--this.$http.delete(url,{'user_pk':user_pk}).then(function(response){--%>
                    <%--console.info(response);--%>
                    <%--_self.userlist = response.body;--%>
                <%--},function(response){--%>
                    <%--console.info(response);--%>
                <%--})--%>
<%--//                this.userlist.splice(pk,1);--%>

                console.log(user_pk);
                var _self = this;
                var url = "${basePath}/user/delete";
                $.ajax({
                    type: 'GET',
                    url: url,
                    data:{user_pk:user_pk},
                    success:function(data) {
                        console.log(data);
                        _self.message = JSON.stringify(data);
                        if(data.result == "true") {
                            _self.showData();
                        }
                    }
                });
            },
            //查询数据
            search_user:function () {
                var _self = this;
                var url = '${basePath}/user/list';
                this.$http.get(url).then(function(response){
                    console.info(response);
                    _self.userlist = response.body;
                },function(response){
                    console.info(response);
                })

            }


        }
    });


    vm.$watch('start', function () {
        $.getJSON("${basePath}/user/userlist", function (result) {
            vm.userlist = result;
        });
    })
</script>

<script>

    layui.use('laydate', function() {
        var laydate = layui.laydate;
    });
    layui.use('form', function(){
        var $ = layui.jquery, form = layui.form;

        //全选
        form.on('checkbox(allChoose)', function(data){
            var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]');
            child.each(function(index, item){
                item.checked = data.elem.checked;
            });
            form.render('checkbox');
        });

    });
</script>
</html>
