package com.weixun.core.config;

/**
 * Created by myd on 2017/5/14.
 */

import com.jfinal.config.*;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.plugin.shiro.ShiroInterceptor;
import com.jfinal.ext.plugin.shiro.ShiroPlugin;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;
import com.jfinal.weixin.sdk.api.ApiConfig;
import com.jfinal.weixin.sdk.api.ApiConfigKit;
import com.weixun.admin.route.AdminRoutes;
import com.weixun.cms.route.CmsRoutes;
import com.weixun.model._MappingKit;
import com.weixun.weixin.route.WeixinRoutes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myd on 2016/12/31.
 */
public class AppConfig extends JFinalConfig {
    private Routes route = null;
    private static List<Routes> routeList = new ArrayList<>();

    /**
     * 如果要支持多公众账号，只需要在此返回各个公众号对应的 ApiConfig 对象即可 可以通过在请求 url 中挂参数来动态从数据库中获取
     * ApiConfig 属性值
     */
    public ApiConfig getApiConfig() {
        ApiConfig ac = new ApiConfig();

        // 配置微信 API 相关常量
        ac.setToken(PropKit.get("token"));
        ac.setAppId(PropKit.get("appId"));
        ac.setAppSecret(PropKit.get("appSecret"));

        /**
         * 是否对消息进行加密，对应于微信平台的消息加解密方式： 1：true进行加密且必须配置 encodingAesKey
         * 2：false采用明文模式，同时也支持混合模式
         */
        ac.setEncryptMessage(PropKit.getBoolean("encryptMessage", false));
        ac.setEncodingAesKey(PropKit.get("encodingAesKey",
                "setting it in config file"));
        return ac;
    }

    /**
     * 如果生产环境配置文件存在，则优先加载该配置，否则加载开发环境配置文件
     * @param pro 生产环境配置文件
     * @param dev 开发环境配置文件
     */
    public void loadProp(String pro, String dev) {
        try {
            PropKit.use(pro);
        }
        catch (Exception e) {
            PropKit.use(dev);
        }
    }

    /**
     * Config constant
     *
     * @param me
     */
    @Override
    public void configConstant(Constants me) {
        PropKit.use("jdbc.properties");


//        loadProp("", "weixin.properties");
        me.setDevMode(PropKit.getBoolean("devMode", false));
        String site_path = PropKit.get("site_path");
        me.setDevMode(true);
        me.setEncoding("UTF-8");
        me.setViewType(ViewType.JSP);
        //文件上传路径
        me.setBaseUploadPath(site_path);
        // ApiConfigKit 设为开发模式可以在开发阶段输出请求交互的 xml 与 json 数据
        ApiConfigKit.setDevMode(me.getDevMode());

    }

    /**
     * Config route
     *
     * @param me
     */
    @Override
    public void configRoute(Routes me) {
        //加入路由list使shiro生效
        routeList.add(new AdminRoutes());
        //加入路由list使shiro生效
        routeList.add(new CmsRoutes());
        //加入admin模块的路由
        me.add(new AdminRoutes());
        //加入cms模块的路由
        me.add(new CmsRoutes());
        //加入weixin（微信）模块的路由
        me.add(new WeixinRoutes());
    }

    @Override
    public void configEngine(Engine engine) {

    }

    /**
     * Config plugin
     *
     * @param plugins
     */
    @Override
    public void configPlugin(Plugins plugins) {
        // DruidPlugin

        final String URL = PropKit.get("jdbcUrl");
        final String USERNAME = PropKit.get("user");
        final String PASSWORD = PropKit.get("password");

        final Integer INITIALSIZE = PropKit.getInt("initialSize");
        final Integer MIDIDLE = PropKit.getInt("minIdle");
        final Integer MAXACTIVEE = PropKit.getInt("maxActivee");

        DruidPlugin druidPlugin = new DruidPlugin(URL,USERNAME,PASSWORD);
        druidPlugin.set(INITIALSIZE,MIDIDLE,MAXACTIVEE);
        druidPlugin.setFilters("stat,wall");
        plugins.add(druidPlugin);

        //多数据源的配置
        ActiveRecordPlugin arp = new ActiveRecordPlugin("datasource",druidPlugin);
        arp.setShowSql(true);//显示sql语句
        plugins.add(arp);
        // 所有配置在 MappingKit 中搞定
        _MappingKit.mapping(arp);

        //配置缓存插件
        plugins.add(new EhCachePlugin());
        //在configPlugin方法内使用
        plugins.add(new ShiroPlugin(routeList));

    }


    /**
     * Config interceptor applied to all actions.
     *
     * @param me
     */
    @Override
    public void configInterceptor(Interceptors me) {
        me.add(new ShiroInterceptor());
    }

    /**
     * Config handler
     *
     * @param me
     */
    @Override
    public void configHandler(Handlers me) {
        me.add(new ContextPathHandler("basePath"));
        DruidStatViewHandler dvh =  new DruidStatViewHandler("/druid");
        me.add(dvh);
    }

    @Override
    public void afterJFinalStart() {
        ApiConfigKit.putApiConfig(getApiConfig());
    }
    /**
     * 需要启动的目录
     * 端口
     * 请求的路径
     * 时间
     * @param args
     */
    public static void main(String[] args)
    {
        JFinal.start("web",8090,"/web",5);
    }
}
